<div>
{{--    @if (isset($_GET['page']))--}}
{{--    {--}}
{{--    {{$page = $_GET['page']}}--}}
{{--    }--}}
{{--    @endif--}}

        @if (session('status'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" style="direction: ltr;">
            <div>
                {{session('status')}}
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
<input wire:model="page"> hello {{$page}}

    <table class="table table-striped" >

        <thead>
        <tr >
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">type</th>
            <th scope="col">email</th>
            <th scope="col">phone</th>
            <th scope="col">title</th>
            <th scope="col">status</th>
            <th scope="col">actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($admins as $admin)
        <tr wire:key="item-{{ $admin->id }}">
            <td>{{$admin->id}}</td>
            @if($editId === $admin->id)
{{--                <td>--}}
{{--                    <input type="hidden" wire:model="page">--}}
{{--                </td>--}}

                <td>
                    <input type="email" class="form-control" wire:model="name">
                    <div class="text-red">@error('name') {{ $message }} @enderror</div>
                </td>


                <td>
                    <select class="form-control" wire:model="type">
                        <option selected>{{$admin->type}}</option>
                        <option>Selected</option>
                        <option value="Super Admin">Super Admin</option>
                        <option value="Admin"> Admin</option>
                    </select>
{{--                    <input type="text" class="form-control" wire:model="type">--}}
                    <div class="text-red">@error('type') {{ $message }} @enderror</div>

                </td>
                <td>
                    <input type="text" class="form-control" wire:model="email">
                    <div class="text-red">@error('email') {{ $message }} @enderror</div>

                </td>

                <td>
                    <input type="text" class="form-control" wire:model="phone">
                </td>

                <td>
                    <input type="text" class="form-control" wire:model="title">
                </td>
                <td>
                    @if($admin->state == 1)
                        <input type="checkbox" wire:click="toggle({{$admin->id}})" checked>
                    @else
                        <input type="checkbox" wire:click="toggle({{$admin->id}})">
                    @endif
                </td>
                <td>
                    <button wire:click.prevent="update" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">تحديث</button>

                        <button wire:click="cancelEdit" class="btn btn-secondary">الغاء</button>

                </td>
            @else
                <td>{{$admin->name}}</td>
                <td>{{$admin->type}}</td>
                <td>{{$admin->phone}}</td>
                <td>{{$admin->title}}</td>
                <td>{{$admin->email}}</td>
                <td>
                    @if($admin->state == 1)
                        <input type="checkbox" wire:click="toggle({{$admin->id}})" checked>
                    @else
                        <input type="checkbox" wire:click="toggle({{$admin->id}})">
                    @endif
                </td>
                <td>
                    <button wire:click="delete({{$admin->id}})" class="btn btn-danger">Delete</button>
                    <button wire:click.prevent="editForm({{$admin->id}})" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        edit
                    </button>
                </td>
            @endif
        </tr>
        @endforeach
        </tbody>

<div class="container">
    <input class="float-right " wire:model.live="search" placeholder="search">

    <div class="float-left">
        {{$admins->links() }}
        <a type="text" class="btn btn-secondary" wire:click="">create</a>
            <button onclick="Livewire.dispatch('openModal', { component: 'Admin.Model'})">Edit User</button>

        {{--        <button onclick="Livewire.emit('openModal', 'Admin.Model')">Edit Userooo</button>--}}


    </div>

</div>
    </table>
</div>
