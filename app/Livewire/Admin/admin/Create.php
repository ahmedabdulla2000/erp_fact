<?php

namespace App\Livewire\Admin\Admin;

use App\Models\admin\Admin;
use Livewire\Attributes\Rule;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Create extends Component
{
    use WithFileUploads;
    use WithPagination;

//    #[Rule('image|max:1024')]
#[Rule('required|max:30|min:3')]
    public $name;
    #[Rule('required|email|max:30|unique:admins')]

    public $email;
#[Rule('required|min:6')]
    public $password;
    #[Rule('required|min:6')]
    public $title;
//    #[Rule('required')]
    public $status;
    #[Rule('required')]
    public $phone;
//    #[Rule('required|max:30|min:6|file')]
//    public $image;

    public function render()
    {
        return view('livewire.admin.admin.create');
    }
    public function save()
    {
        $this->validate();
        Admin::create(
            $this->only(['name', 'phone' ,'email', 'password' , 'title' , 'status'])
        );
request()->session()->flash('status' , 'added');
        return $this->redirect('/admin/admins');
    }

}
