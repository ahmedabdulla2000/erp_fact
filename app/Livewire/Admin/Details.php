<?php

namespace App\Livewire\Admin;

use Livewire\Component;

class Details extends Component
{

    public function show()
    {
        return view('livewire.admin.details');
    }
    public function render()
    {
        return view('livewire.admin.details');
    }
}
