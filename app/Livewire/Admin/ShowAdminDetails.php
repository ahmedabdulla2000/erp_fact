<?php

namespace App\Livewire\Admin;

use App\Models\admin\Admin;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ShowAdminDetails extends Component
{
//    public function render()
//    {
////        dd('render');
//        return view('livewire.admin.admin-details');
//    }



    public function show()
    {
        $getId = Admin::where('id' , Auth::guard('admin')->user()->id)->first();
        return view('livewire.admin.show-admin-details' , ['getId' => $getId]);
    }

    public function edit()
    {
        $getId = Admin::where('id' , Auth::guard('admin')->user()->id)->first();
        return view('livewire.admin.admin-details' , ['getId' => $getId]);
    }

}
