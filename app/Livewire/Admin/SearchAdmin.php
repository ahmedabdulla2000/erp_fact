<?php

namespace App\Livewire\Admin;

use App\Models\admin\Admin;
use Livewire\Component;

class SearchAdmin extends Component
{
    public $search = '';
    public function render()
    {

        return view('livewire.admin.admins' , [
            'searchs' => Admin::search($this->search)
        ]);
    }
}
