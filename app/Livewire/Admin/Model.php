<?php

namespace App\Livewire\Admin;

use LivewireUI\Modal\ModalComponent;

class Model extends ModalComponent
{
    public static function modalMaxWidth(): string
    {
        // 'sm'
        // 'md'
        // 'lg'
        // 'xl'
        // '2xl'
        // '3xl'
        // '4xl'
        // '5xl'
        // '6xl'
        // '7xl'
        return '4xl';
    }
    public function render()
    {
        return view('livewire.admin.model');
    }
}
