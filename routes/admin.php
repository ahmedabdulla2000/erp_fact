<?php

use App\Http\Controllers\Admin\AdminDetailController;
use App\Livewire\Admin\Details;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


//Route::namespace('\App\Http\Controllers\Admin')->prefix('admin')->group(function (){
//    Route::resource('admin' , AdminController::class);
//});


Route::name('admin.')->prefix('admin')->group(function (){
    Route::middleware('Admin')->group(function (){
        Route::namespace('\App\Http\Controllers\Admin')->group(function (){
            Route::view('/dashboard' , 'admin.index')->name('dashboard');
//            Route::resource('/details' , AdminDetailController::class);
        });

        Route::namespace('\App\Livewire\Admin')->group(function (){
            Route::get('createAdmin' , function (){
                return view('admin.admins.create');
 });

            Route::get('admins' , function (){
                return view('admin.admins.index' , ['name' => 'admins']);
            });

            Route::resource('details' , Details::class);
//            Route::resource('show_details' , ShowAdminDetails::class);
//            Route::resource('show_details' , ShowAdminDetails::class);
        });


    });


    require __DIR__.'/admin_auth.php';
});



//Route::get('/dashboard', function () {
//    return view('dashboard');
//})->middleware(['auth', 'verified'])->name('dashboard');
//
//Route::middleware('auth')->group(function () {
//    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
//});


