<?php

namespace Database\Seeders;

use App\Models\admin\Admin;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $adminRecord = [
            [    'id' => 1 ,
                'name' => 'Super Admin' ,
                'title' => 'Cairo' ,
                'type' => 'super admin' ,
                'email' => 'ahmed.git@yahoo.com' ,
                'phone' => '01016070906' ,
                'password' => '$2y$10$S4L2FmBintjFMivTNMm94ub2dBE174Eob4g2CUJbE2Q4J81Ddm2YG',
                'state' => 1 ],
            ['id' => 2 , 'name' => 'admin1' , 'title' => 'Cairo' , 'type' => 'admin' ,  'password' => '$2y$10$S4L2FmBintjFMivTNMm94ub2dBE174Eob4g2CUJbE2Q4J81Ddm2YG'
                ,'email' => 'admin1@yahoo.com' , 'phone' => '01016070906' , 'state' => 0],
            ['id' => 3 , 'name' => 'admin2' , 'title' => 'Cairo' , 'type' => 'admin' , 'phone' => '01016070906' ,  'password' => '$2y$10$S4L2FmBintjFMivTNMm94ub2dBE174Eob4g2CUJbE2Q4J81Ddm2YG'
                ,'email' => 'admin2@yahoo.com' , 'state' => 0],
        ];
        Admin::insert($adminRecord);
    }
}
